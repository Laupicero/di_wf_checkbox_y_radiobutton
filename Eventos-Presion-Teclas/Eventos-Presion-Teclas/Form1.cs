﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Eventos_Presion_Teclas
{
    public partial class Eventos : Form
    {
        public Eventos()
        {
            InitializeComponent();
        }

        //Cuando se presiona una tecla por primera vez
        private void tb1_KeyDown(object sender, KeyEventArgs e)
        {
            this.tb_Estado.Text = "keyDown";
        }

        //Mientras la tecla está presionada
        private void tb1_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.tb_Estado.Text = "keyPress";
            this.tb_LetraPulsada.Text = e.KeyChar.ToString();

            if (e.KeyChar == (char)Keys.Enter)
            {
                this.tb_Result.Text = this.tb1.Text;
            }
        }

        //Cuando se suelta la tecla presionada
        private void tb1_KeyUp(object sender, KeyEventArgs e)
        {
            this.tb_Estado.Text = "keyUp";
            this.tb_TeclaEspecial.Text = e.KeyData.ToString();


        }
    }
}
