﻿namespace Eventos_Presion_Teclas
{
    partial class Eventos
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb1 = new System.Windows.Forms.TextBox();
            this.labelInfo = new System.Windows.Forms.Label();
            this.tb_Estado = new System.Windows.Forms.TextBox();
            this.tb_LetraPulsada = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_TeclaEspecial = new System.Windows.Forms.TextBox();
            this.lb2 = new System.Windows.Forms.Label();
            this.tb_Result = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tb1
            // 
            this.tb1.Location = new System.Drawing.Point(35, 65);
            this.tb1.Multiline = true;
            this.tb1.Name = "tb1";
            this.tb1.Size = new System.Drawing.Size(502, 104);
            this.tb1.TabIndex = 0;
            this.tb1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb1_KeyDown);
            this.tb1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb1_KeyPress);
            this.tb1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb1_KeyUp);
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfo.Location = new System.Drawing.Point(66, 181);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(98, 16);
            this.labelInfo.TabIndex = 1;
            this.labelInfo.Text = "Estado \'key\':";
            // 
            // tb_Estado
            // 
            this.tb_Estado.Enabled = false;
            this.tb_Estado.Location = new System.Drawing.Point(258, 177);
            this.tb_Estado.Name = "tb_Estado";
            this.tb_Estado.Size = new System.Drawing.Size(170, 20);
            this.tb_Estado.TabIndex = 2;
            // 
            // tb_LetraPulsada
            // 
            this.tb_LetraPulsada.Enabled = false;
            this.tb_LetraPulsada.Location = new System.Drawing.Point(258, 203);
            this.tb_LetraPulsada.Name = "tb_LetraPulsada";
            this.tb_LetraPulsada.Size = new System.Drawing.Size(170, 20);
            this.tb_LetraPulsada.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(66, 207);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Letra pulsada (keyPress):";
            // 
            // tb_TeclaEspecial
            // 
            this.tb_TeclaEspecial.Enabled = false;
            this.tb_TeclaEspecial.Location = new System.Drawing.Point(258, 229);
            this.tb_TeclaEspecial.Name = "tb_TeclaEspecial";
            this.tb_TeclaEspecial.Size = new System.Drawing.Size(170, 20);
            this.tb_TeclaEspecial.TabIndex = 6;
            // 
            // lb2
            // 
            this.lb2.AutoSize = true;
            this.lb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb2.Location = new System.Drawing.Point(66, 233);
            this.lb2.Name = "lb2";
            this.lb2.Size = new System.Drawing.Size(171, 16);
            this.lb2.TabIndex = 5;
            this.lb2.Text = "Tecla pulsada (keyUp):";
            // 
            // tb_Result
            // 
            this.tb_Result.Enabled = false;
            this.tb_Result.Location = new System.Drawing.Point(170, 291);
            this.tb_Result.Name = "tb_Result";
            this.tb_Result.Size = new System.Drawing.Size(367, 20);
            this.tb_Result.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(81, 295);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Resultado:";
            // 
            // Eventos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 342);
            this.Controls.Add(this.tb_Result);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_TeclaEspecial);
            this.Controls.Add(this.lb2);
            this.Controls.Add(this.tb_LetraPulsada);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_Estado);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.tb1);
            this.Name = "Eventos";
            this.Text = "Eventos de Teclado";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb1;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.TextBox tb_Estado;
        private System.Windows.Forms.TextBox tb_LetraPulsada;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_TeclaEspecial;
        private System.Windows.Forms.Label lb2;
        private System.Windows.Forms.TextBox tb_Result;
        private System.Windows.Forms.Label label2;
    }
}

