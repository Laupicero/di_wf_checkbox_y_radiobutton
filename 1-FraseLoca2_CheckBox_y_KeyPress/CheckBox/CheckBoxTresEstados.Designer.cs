﻿namespace CheckBox
{
    partial class CheckBoxTresEstados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbCambioFormato2 = new System.Windows.Forms.CheckBox();
            this.tbIntroduccion = new System.Windows.Forms.TextBox();
            this.btnAtras = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Conversor de Texto";
            // 
            // cbCambioFormato2
            // 
            this.cbCambioFormato2.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbCambioFormato2.AutoSize = true;
            this.cbCambioFormato2.Checked = true;
            this.cbCambioFormato2.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.cbCambioFormato2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.cbCambioFormato2.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.cbCambioFormato2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCambioFormato2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCambioFormato2.Location = new System.Drawing.Point(25, 211);
            this.cbCambioFormato2.Name = "cbCambioFormato2";
            this.cbCambioFormato2.Size = new System.Drawing.Size(82, 28);
            this.cbCambioFormato2.TabIndex = 4;
            this.cbCambioFormato2.Text = "CAMBIO";
            this.cbCambioFormato2.UseVisualStyleBackColor = true;
            // 
            // tbIntroduccion
            // 
            this.tbIntroduccion.Location = new System.Drawing.Point(25, 88);
            this.tbIntroduccion.Multiline = true;
            this.tbIntroduccion.Name = "tbIntroduccion";
            this.tbIntroduccion.Size = new System.Drawing.Size(539, 97);
            this.tbIntroduccion.TabIndex = 3;
            this.tbIntroduccion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIntroduccion_KeyPress);
            // 
            // btnAtras
            // 
            this.btnAtras.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnAtras.FlatAppearance.BorderSize = 2;
            this.btnAtras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtras.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtras.Location = new System.Drawing.Point(480, 263);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(130, 37);
            this.btnAtras.TabIndex = 6;
            this.btnAtras.Text = "ATRÁS";
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // CheckBoxTresEstados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 312);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbCambioFormato2);
            this.Controls.Add(this.tbIntroduccion);
            this.Name = "CheckBoxTresEstados";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Versión mejorada";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbCambioFormato2;
        private System.Windows.Forms.TextBox tbIntroduccion;
        private System.Windows.Forms.Button btnAtras;
    }
}