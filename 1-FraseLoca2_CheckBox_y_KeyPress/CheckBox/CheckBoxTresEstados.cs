﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckBox
{
    public partial class CheckBoxTresEstados : Form
    {
        public CheckBoxTresEstados()
        {
            InitializeComponent();
            cbCambioFormato2.CheckState = CheckState.Indeterminate;
        }

        
        //Método que nos devuelve al formulario principal
        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
        //Nueva versión mejoada donde nos transformara a mayúscula, minúscula o ambas dependiendo
        // del estado del botón
        private void tbIntroduccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            String check = cbCambioFormato2.CheckState.ToString();
            //Check box pulsado
            if (cbCambioFormato2.CheckState == CheckState.Checked)
            {
                e.KeyChar  = char.ToUpper(e.KeyChar);
                this.cbCambioFormato2.BackColor = Color.Green;
            }

            //Dejamos de pulsar el checkbox
            if (cbCambioFormato2.CheckState == CheckState.Unchecked)
            {
                e.KeyChar = char.ToLower(e.KeyChar);
                this.cbCambioFormato2.BackColor = Color.Red;
            }

            //Estado 'indeterminado'
            if (cbCambioFormato2.CheckState == CheckState.Indeterminate)
            {
                this.cbCambioFormato2.BackColor = Color.Gray;
            }
        }
    }
}
