﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckBox
{
    public partial class ConversorCheckBox : Form
    {
        private String txt;     //Variable para capturar/guardar el contenido del textBox
        private CheckBoxTresEstados formTresEstados;    //Nos servirá para abrir la ventana del otro formulario

        //Cosntructor
        public ConversorCheckBox()
        {
            InitializeComponent();
            this.formTresEstados = new CheckBoxTresEstados();
            this.txt = "Texting my Test";
            tbIntroduccion.Text = txt;
        }

        private void cbCambioFormato_CheckedChanged(object sender, EventArgs e)
        {
            if(tbIntroduccion.TextLength >0 && tbIntroduccion.Text != null)
            {
                if (cbCambioFormato.Checked)
                {
                    txt = tbIntroduccion.Text;
                    tbIntroduccion.Text = txt.ToUpper();
                }
                else
                {
                    tbIntroduccion.Text = txt;
                }
            }
            else
            {
                MessageBox.Show("DEBE INTRODUCIR PRIMERO CUALQUIER TEXTO","ERROR");
            }
        }

        
        //Métod que nos servirá para ir al siguiente formulario
        private void btnSiguienteFormulario_Click(object sender, EventArgs e)
        {
            this.formTresEstados.ShowDialog();
        }

        //Botón Salir
        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }
    }
}
