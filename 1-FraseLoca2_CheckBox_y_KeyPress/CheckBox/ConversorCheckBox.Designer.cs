﻿namespace CheckBox
{
    partial class ConversorCheckBox
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbIntroduccion = new System.Windows.Forms.TextBox();
            this.cbCambioFormato = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSiguienteFormulario = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbIntroduccion
            // 
            this.tbIntroduccion.Location = new System.Drawing.Point(38, 91);
            this.tbIntroduccion.Name = "tbIntroduccion";
            this.tbIntroduccion.Size = new System.Drawing.Size(422, 20);
            this.tbIntroduccion.TabIndex = 0;
            // 
            // cbCambioFormato
            // 
            this.cbCambioFormato.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbCambioFormato.AutoSize = true;
            this.cbCambioFormato.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.cbCambioFormato.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cbCambioFormato.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCambioFormato.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCambioFormato.Location = new System.Drawing.Point(38, 131);
            this.cbCambioFormato.Name = "cbCambioFormato";
            this.cbCambioFormato.Size = new System.Drawing.Size(82, 28);
            this.cbCambioFormato.TabIndex = 1;
            this.cbCambioFormato.Text = "CAMBIO";
            this.cbCambioFormato.UseVisualStyleBackColor = true;
            this.cbCambioFormato.CheckedChanged += new System.EventHandler(this.cbCambioFormato_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Conversor de Texto en Mayúsculas";
            // 
            // btnSiguienteFormulario
            // 
            this.btnSiguienteFormulario.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnSiguienteFormulario.FlatAppearance.BorderSize = 2;
            this.btnSiguienteFormulario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguienteFormulario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSiguienteFormulario.Location = new System.Drawing.Point(42, 241);
            this.btnSiguienteFormulario.Name = "btnSiguienteFormulario";
            this.btnSiguienteFormulario.Size = new System.Drawing.Size(196, 56);
            this.btnSiguienteFormulario.TabIndex = 3;
            this.btnSiguienteFormulario.Text = "SIGUIENTE FORMULARIO";
            this.btnSiguienteFormulario.UseVisualStyleBackColor = true;
            this.btnSiguienteFormulario.Click += new System.EventHandler(this.btnSiguienteFormulario_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnSalir.FlatAppearance.BorderSize = 2;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Location = new System.Drawing.Point(298, 241);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(196, 56);
            this.btnSalir.TabIndex = 4;
            this.btnSalir.Text = "SALIR";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // ConversorCheckBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.OldLace;
            this.ClientSize = new System.Drawing.Size(549, 309);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnSiguienteFormulario);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbCambioFormato);
            this.Controls.Add(this.tbIntroduccion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ConversorCheckBox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conversor de texto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbIntroduccion;
        private System.Windows.Forms.CheckBox cbCambioFormato;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSiguienteFormulario;
        private System.Windows.Forms.Button btnSalir;
    }
}

