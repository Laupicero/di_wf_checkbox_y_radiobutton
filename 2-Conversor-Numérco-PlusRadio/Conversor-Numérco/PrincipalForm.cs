﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Conversor_Numérco
{
    public partial class PrincipalForm : Form
    {
        //Atributo en el que guardaremos el número que hayamos introducido para hacer, futuramente, las futuras comprobaciones
        private String numInput;
        
        //Constructor
        public PrincipalForm()
        {
            InitializeComponent();
            this.numInput = "0";
        }

        // -------------------------------------------------------
        // Evetos de los botones CONVERTIR y SALIR
        // -------------------------------------------------------
    
        //Salir del programa
        private void btnSALIR_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }


        //Botón convertir
        private void btnConvertir_Click(object sender, EventArgs e)
        {
            //Comprobamos que 'radioButton' está activo
            if (rbDecimal.Checked)
            {
                //Guardamos el 'string' del número introducido
                this.numInput = textBoxNumDecimal.Text;

                //Comprobamos si hay texto escrito en el 'textBox' correspondiente y usaremos unas expresiones 'Regex' de una clase
                // que tendremos aparte para ayudarnos a comprobar si los números que hemos introducido están en el formato correcto
                if (textBoxNumDecimal.TextLength >= 0 && RegexUtilsNumeros.esNumDecimal(this.numInput))
                {
                    textBoxNumDecimal.Text = this.numInput;
                    textBoxBinario.Text = Convert.ToString(Convert.ToInt32(this.numInput), 2);
                    textBoxOctal.Text = Convert.ToString(Convert.ToInt32(this.numInput), 8); ;
                    textBoxHexadecimal.Text = Convert.ToString(Convert.ToInt32(this.numInput), 16);
                }
                //Mensaje de error
                else
                {
                    MessageBox.Show("Por favor, inserte un número válido", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            // Opción número binario
            else if (rbBinario.Checked)
            {
                //Guardamos el 'string' del número introducido
                this.numInput = textBoxBinario.Text;

                if (textBoxBinario.TextLength >= 0 && RegexUtilsNumeros.esNumBinario(this.numInput))
                {
                    textBoxNumDecimal.Text = Convert.ToInt32(this.numInput, 2).ToString();
                    textBoxBinario.Text = this.numInput;
                    textBoxOctal.Text = Convert.ToString(Convert.ToInt32(textBoxNumDecimal.Text), 8); ;
                    textBoxHexadecimal.Text = Convert.ToString(Convert.ToInt32(textBoxNumDecimal.Text), 16);
                }
                else
                {
                    MessageBox.Show("Por favor, inserte un número válido", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            // Opción número octal
            else if (rbOctal.Checked)
            {
                this.numInput = textBoxOctal.Text;

                if (textBoxOctal.TextLength >= 0 && RegexUtilsNumeros.esNumOctal(this.numInput))
                {
                    textBoxNumDecimal.Text = Convert.ToInt32(this.numInput, 8).ToString();
                    textBoxOctal.Text = this.numInput;
                    textBoxBinario.Text = Convert.ToString(Convert.ToInt32(textBoxNumDecimal.Text), 2); ;
                    textBoxHexadecimal.Text = Convert.ToString(Convert.ToInt32(textBoxNumDecimal.Text), 16);
                }
                else
                {
                    MessageBox.Show("Por favor, inserte un número válido", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            // Opción número hexadecimal
            else if (rbHexadecimal.Checked)
            {
                this.numInput = textBoxHexadecimal.Text;

                if (textBoxHexadecimal.TextLength >= 0 && RegexUtilsNumeros.esNumHexadecimal(this.numInput))
                {
                    textBoxNumDecimal.Text = Convert.ToInt32(this.numInput, 16).ToString();
                    textBoxHexadecimal.Text = this.numInput;
                    textBoxBinario.Text = Convert.ToString(Convert.ToInt32(textBoxNumDecimal.Text), 2); ;
                    textBoxOctal.Text = Convert.ToString(Convert.ToInt32(textBoxNumDecimal.Text), 8);
                }
                else
                {
                    MessageBox.Show("Por favor, inserte un número válido", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Seleccione primero una de las diversas formas " +
                    "\nen la que desea insertar su número (Decimal, Binario...)", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }//Fi botón convertir


        //------------------------------
        // Eventos de los 'radioButton' que nos activará el 'textBox' correspondiente a la opción numérica que hemos pulsado. Ej:
        // Si pulsamos el 'radioButton' que contiene 'binario' activará el 'textBox' en el que sólo podremos introducir números binarios
        //------------------------------

        //Nos activará el 'textbox' de los números decimales
        private void rbDecimal_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDecimal.Checked)
            {
                borrarTextBox();
                DeshabilitarBotones();
                textBoxNumDecimal.Enabled = true;

            }
        }

        //Nos activará el 'textbox' de los números binarios
        private void rbBinario_CheckedChanged(object sender, EventArgs e)
        {
            if (rbBinario.Checked)
            {
                borrarTextBox();
                DeshabilitarBotones();
                textBoxBinario.Enabled = true;

            }
        }

        //Nos activará el 'textbox' de los números octales
        private void rbOctal_CheckedChanged(object sender, EventArgs e)
        {
            if (rbOctal.Checked)
            {
                this.numInput = textBoxOctal.Text;
                borrarTextBox();
                DeshabilitarBotones();
                textBoxOctal.Enabled = true;

            }
        }

        //Nos activará el 'textbox' de los números hexadecimales
        private void rbHexadecimal_CheckedChanged(object sender, EventArgs e)
        {
            if (rbHexadecimal.Checked)
            {
                borrarTextBox();
                DeshabilitarBotones();
                textBoxHexadecimal.Enabled = true;

            }
        }



        //--------------------------------------
        // Métodos de ayuda que nos ayudará a tener un mejor control sobre el formulario
        //--------------------------------------
        //Borrar todos los TextBox
        private void borrarTextBox()
        {
            textBoxNumDecimal.Clear();
            textBoxBinario.Clear();
            textBoxOctal.Clear();
            textBoxHexadecimal.Clear();
        }


        //Opción que nos deshabilitarán todos los botones
        private void DeshabilitarBotones()
        {
            textBoxNumDecimal.Enabled = false;
            textBoxBinario.Enabled = false;
            textBoxOctal.Enabled = false;
            textBoxHexadecimal.Enabled = false;
        }
    }
}
