﻿namespace Conversor_Numérco
{
    partial class PrincipalForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrincipalForm));
            this.labelTituloConversorNumerico = new System.Windows.Forms.Label();
            this.textBoxNumDecimal = new System.Windows.Forms.TextBox();
            this.textBoxBinario = new System.Windows.Forms.TextBox();
            this.textBoxOctal = new System.Windows.Forms.TextBox();
            this.textBoxHexadecimal = new System.Windows.Forms.TextBox();
            this.pictureBoxBinario = new System.Windows.Forms.PictureBox();
            this.btnSALIR = new System.Windows.Forms.Button();
            this.gbControlerRadiobtn = new System.Windows.Forms.GroupBox();
            this.rbHexadecimal = new System.Windows.Forms.RadioButton();
            this.rbOctal = new System.Windows.Forms.RadioButton();
            this.rbBinario = new System.Windows.Forms.RadioButton();
            this.rbDecimal = new System.Windows.Forms.RadioButton();
            this.btnConvertir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBinario)).BeginInit();
            this.gbControlerRadiobtn.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTituloConversorNumerico
            // 
            this.labelTituloConversorNumerico.AutoSize = true;
            this.labelTituloConversorNumerico.Font = new System.Drawing.Font("Microsoft Sans Serif", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTituloConversorNumerico.Location = new System.Drawing.Point(51, 30);
            this.labelTituloConversorNumerico.Name = "labelTituloConversorNumerico";
            this.labelTituloConversorNumerico.Size = new System.Drawing.Size(662, 76);
            this.labelTituloConversorNumerico.TabIndex = 0;
            this.labelTituloConversorNumerico.Text = "Conversor Numérico";
            // 
            // textBoxNumDecimal
            // 
            this.textBoxNumDecimal.Enabled = false;
            this.textBoxNumDecimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNumDecimal.Location = new System.Drawing.Point(216, 49);
            this.textBoxNumDecimal.Name = "textBoxNumDecimal";
            this.textBoxNumDecimal.Size = new System.Drawing.Size(213, 29);
            this.textBoxNumDecimal.TabIndex = 5;
            this.textBoxNumDecimal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxBinario
            // 
            this.textBoxBinario.Enabled = false;
            this.textBoxBinario.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBinario.Location = new System.Drawing.Point(216, 113);
            this.textBoxBinario.Name = "textBoxBinario";
            this.textBoxBinario.Size = new System.Drawing.Size(213, 29);
            this.textBoxBinario.TabIndex = 6;
            this.textBoxBinario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxOctal
            // 
            this.textBoxOctal.Enabled = false;
            this.textBoxOctal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOctal.Location = new System.Drawing.Point(216, 178);
            this.textBoxOctal.Name = "textBoxOctal";
            this.textBoxOctal.Size = new System.Drawing.Size(213, 29);
            this.textBoxOctal.TabIndex = 7;
            this.textBoxOctal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxHexadecimal
            // 
            this.textBoxHexadecimal.Enabled = false;
            this.textBoxHexadecimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHexadecimal.Location = new System.Drawing.Point(216, 243);
            this.textBoxHexadecimal.Name = "textBoxHexadecimal";
            this.textBoxHexadecimal.Size = new System.Drawing.Size(213, 29);
            this.textBoxHexadecimal.TabIndex = 8;
            this.textBoxHexadecimal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // pictureBoxBinario
            // 
            this.pictureBoxBinario.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxBinario.Image")));
            this.pictureBoxBinario.Location = new System.Drawing.Point(545, 128);
            this.pictureBoxBinario.Name = "pictureBoxBinario";
            this.pictureBoxBinario.Size = new System.Drawing.Size(213, 432);
            this.pictureBoxBinario.TabIndex = 10;
            this.pictureBoxBinario.TabStop = false;
            // 
            // btnSALIR
            // 
            this.btnSALIR.BackColor = System.Drawing.Color.Green;
            this.btnSALIR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSALIR.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSALIR.Location = new System.Drawing.Point(38, 512);
            this.btnSALIR.Name = "btnSALIR";
            this.btnSALIR.Size = new System.Drawing.Size(465, 48);
            this.btnSALIR.TabIndex = 11;
            this.btnSALIR.Text = "SALIR";
            this.btnSALIR.UseVisualStyleBackColor = false;
            this.btnSALIR.Click += new System.EventHandler(this.btnSALIR_Click);
            // 
            // gbControlerRadiobtn
            // 
            this.gbControlerRadiobtn.Controls.Add(this.rbHexadecimal);
            this.gbControlerRadiobtn.Controls.Add(this.rbOctal);
            this.gbControlerRadiobtn.Controls.Add(this.rbBinario);
            this.gbControlerRadiobtn.Controls.Add(this.rbDecimal);
            this.gbControlerRadiobtn.Controls.Add(this.textBoxHexadecimal);
            this.gbControlerRadiobtn.Controls.Add(this.textBoxNumDecimal);
            this.gbControlerRadiobtn.Controls.Add(this.textBoxOctal);
            this.gbControlerRadiobtn.Controls.Add(this.textBoxBinario);
            this.gbControlerRadiobtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbControlerRadiobtn.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbControlerRadiobtn.Location = new System.Drawing.Point(47, 128);
            this.gbControlerRadiobtn.Name = "gbControlerRadiobtn";
            this.gbControlerRadiobtn.Size = new System.Drawing.Size(456, 304);
            this.gbControlerRadiobtn.TabIndex = 13;
            this.gbControlerRadiobtn.TabStop = false;
            // 
            // rbHexadecimal
            // 
            this.rbHexadecimal.AutoSize = true;
            this.rbHexadecimal.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbHexadecimal.ForeColor = System.Drawing.Color.SteelBlue;
            this.rbHexadecimal.Location = new System.Drawing.Point(49, 243);
            this.rbHexadecimal.Name = "rbHexadecimal";
            this.rbHexadecimal.Size = new System.Drawing.Size(149, 29);
            this.rbHexadecimal.TabIndex = 3;
            this.rbHexadecimal.TabStop = true;
            this.rbHexadecimal.Text = "Hexadecimal";
            this.rbHexadecimal.UseVisualStyleBackColor = true;
            this.rbHexadecimal.CheckedChanged += new System.EventHandler(this.rbHexadecimal_CheckedChanged);
            // 
            // rbOctal
            // 
            this.rbOctal.AutoSize = true;
            this.rbOctal.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbOctal.ForeColor = System.Drawing.Color.SteelBlue;
            this.rbOctal.Location = new System.Drawing.Point(49, 178);
            this.rbOctal.Name = "rbOctal";
            this.rbOctal.Size = new System.Drawing.Size(78, 29);
            this.rbOctal.TabIndex = 2;
            this.rbOctal.TabStop = true;
            this.rbOctal.Text = "Octal";
            this.rbOctal.UseVisualStyleBackColor = true;
            this.rbOctal.CheckedChanged += new System.EventHandler(this.rbOctal_CheckedChanged);
            // 
            // rbBinario
            // 
            this.rbBinario.AutoSize = true;
            this.rbBinario.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbBinario.ForeColor = System.Drawing.Color.SteelBlue;
            this.rbBinario.Location = new System.Drawing.Point(49, 113);
            this.rbBinario.Name = "rbBinario";
            this.rbBinario.Size = new System.Drawing.Size(94, 29);
            this.rbBinario.TabIndex = 1;
            this.rbBinario.TabStop = true;
            this.rbBinario.Text = "Binario";
            this.rbBinario.UseVisualStyleBackColor = true;
            this.rbBinario.CheckedChanged += new System.EventHandler(this.rbBinario_CheckedChanged);
            // 
            // rbDecimal
            // 
            this.rbDecimal.AutoSize = true;
            this.rbDecimal.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbDecimal.ForeColor = System.Drawing.Color.SteelBlue;
            this.rbDecimal.Location = new System.Drawing.Point(49, 49);
            this.rbDecimal.Name = "rbDecimal";
            this.rbDecimal.Size = new System.Drawing.Size(104, 29);
            this.rbDecimal.TabIndex = 0;
            this.rbDecimal.TabStop = true;
            this.rbDecimal.Text = "Decimal";
            this.rbDecimal.UseVisualStyleBackColor = true;
            this.rbDecimal.CheckedChanged += new System.EventHandler(this.rbDecimal_CheckedChanged);
            // 
            // btnConvertir
            // 
            this.btnConvertir.BackColor = System.Drawing.Color.Black;
            this.btnConvertir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConvertir.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnConvertir.Location = new System.Drawing.Point(38, 458);
            this.btnConvertir.Name = "btnConvertir";
            this.btnConvertir.Size = new System.Drawing.Size(465, 48);
            this.btnConvertir.TabIndex = 14;
            this.btnConvertir.Text = "CONVERTIR";
            this.btnConvertir.UseVisualStyleBackColor = false;
            this.btnConvertir.Click += new System.EventHandler(this.btnConvertir_Click);
            // 
            // PrincipalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 572);
            this.Controls.Add(this.btnConvertir);
            this.Controls.Add(this.gbControlerRadiobtn);
            this.Controls.Add(this.btnSALIR);
            this.Controls.Add(this.pictureBoxBinario);
            this.Controls.Add(this.labelTituloConversorNumerico);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrincipalForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conversor de tipos";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBinario)).EndInit();
            this.gbControlerRadiobtn.ResumeLayout(false);
            this.gbControlerRadiobtn.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTituloConversorNumerico;
        private System.Windows.Forms.TextBox textBoxNumDecimal;
        private System.Windows.Forms.TextBox textBoxBinario;
        private System.Windows.Forms.TextBox textBoxOctal;
        private System.Windows.Forms.TextBox textBoxHexadecimal;
        private System.Windows.Forms.PictureBox pictureBoxBinario;
        private System.Windows.Forms.Button btnSALIR;
        private System.Windows.Forms.GroupBox gbControlerRadiobtn;
        private System.Windows.Forms.RadioButton rbHexadecimal;
        private System.Windows.Forms.RadioButton rbOctal;
        private System.Windows.Forms.RadioButton rbBinario;
        private System.Windows.Forms.RadioButton rbDecimal;
        private System.Windows.Forms.Button btnConvertir;
    }
}

