﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Conversor_Numérco
{
    /// <summary>
    /// Clase con métodos estáticos que contendrá 4 expresiones regulares que nos ayudará a comprobar que
    /// los número que hayamos introducido están en el formato adecuado
    /// </summary>
    class RegexUtilsNumeros
    {
        
        /// <summary>
        /// Nos devolverá una variable de tipo 'boolean' en el que:
        /// true: El número introducido es correcto
        /// false: El número introducido no es decimal
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static Boolean esNumDecimal(String num)
        {
            String reg = "^[0-9]+$";
            Regex expresionRegular = new Regex(reg);
            return expresionRegular.IsMatch(num);
        }


        /// <summary>
        /// Nos devolverá una variable de tipo 'boolean' en el que:
        /// true: El número introducido es correcto
        /// false: El número introducido no es binario
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static Boolean esNumBinario(String num)
        {
            String reg = @"^[01]+$";
            Regex expresionRegular = new Regex(reg);
            return expresionRegular.IsMatch(num);
        }


        /// <summary>
        /// Nos devolverá una variable de tipo 'boolean' en el que:
        /// true: El número introducido es correcto
        /// false: El número introducido no es octal
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static Boolean esNumOctal(String num)
        {
            String reg = "^[0-7]+$";
            Regex expresionRegular = new Regex(reg);
            return expresionRegular.IsMatch(num);
        }


        /// <summary>
        /// Nos devolverá una variable de tipo 'boolean' en el que:
        /// true: El número introducido es correcto
        /// false: El número introducido no es Hexadecimal
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static Boolean esNumHexadecimal(String num)
        {
            String reg = "[0-9a-fA-F]";
            Regex expresionRegular = new Regex(reg);
            return expresionRegular.IsMatch(num);
        }
    }
}
